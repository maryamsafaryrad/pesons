import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { InsertpersonComponent } from './insertperson/insertperson.component';
import {FormsModule} from "@angular/forms";
import { ChatComponent } from './chat/chat.component';
import {RouterModule} from "@angular/router";
import {AppRoutingModule} from "./app.routing";
import { ManagepersonComponent } from './manageperson/manageperson.component';
import {HttpClient, HttpClientModule} from "@angular/common/http";



@NgModule({
  declarations: [
    AppComponent,
    InsertpersonComponent,
    ChatComponent,
    ManagepersonComponent
  ],
    imports: [
      BrowserModule,
      FormsModule,
      AppRoutingModule,
      HttpClientModule,
      RouterModule
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
