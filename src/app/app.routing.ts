import {RouterModule, Routes} from "@angular/router";
import {InsertpersonComponent} from "./insertperson/insertperson.component";
import {ChatComponent} from "./chat/chat.component";
import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {BrowserModule} from "@angular/platform-browser";
import {ManagepersonComponent} from "./manageperson/manageperson.component";

const routes: Routes=[
  {
    path:'',
    redirectTo:'manageperson',
    pathMatch:'full'
  },
  {
    path:'insert',
    component: InsertpersonComponent,
    pathMatch: 'full'
  },
  {
    path:'chat',
    component: ChatComponent,
    pathMatch:'full'
  },
  {
    path:'manageperson',
    component: ManagepersonComponent ,
    pathMatch:'full'
  }
]
@NgModule({
  imports:[
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes)
  ],
  exports :[
    RouterModule
  ],
  declarations:[]
})
export class AppRoutingModule {


}
