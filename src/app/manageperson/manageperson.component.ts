import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {RestClientService} from "../rest-client.service";

@Component({
  selector: 'app-manageperson',
  templateUrl: './manageperson.component.html',
  styleUrls: ['./manageperson.component.css']
})
export class ManagepersonComponent implements OnInit {

  constructor(private router:Router,private restclient:RestClientService) { }
  list=[];
  gotoinsertt()
  {
    this.router.navigateByUrl('/insert');
  }
  ngOnInit(): void {
    this.restclient.getEmployee().subscribe(res=>{
      console.log("list",res);
      this.list=res.data;
    })
  }

}
