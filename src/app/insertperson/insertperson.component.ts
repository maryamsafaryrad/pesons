import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {Employee} from "./employee";
import {RestClientService} from "../rest-client.service";

@Component({
  selector: 'app-insertperson',
  templateUrl: './insertperson.component.html',
  styleUrls: ['./insertperson.component.css']
})
export class InsertpersonComponent implements OnInit {

  constructor(private router:Router,private restclient:RestClientService) { }
list:[];
  employee:Employee;

  ngOnInit() {
    this.restclient.getEmployee().subscribe(res=>{
      console.log("list",res);
      this.list=res.data;
    })
     this.employee={name:'',salary:null,age:null,id:null};
  }
    gotochat() {
      this.router.navigateByUrl('/chat');
    }
  gotomanageperson()
  {
    this.router.navigateByUrl('/manageperson');

  }


}
